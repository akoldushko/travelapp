//
//  TripsViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import RealmSwift

class TripsViewController: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    var trips = [Trip]()
    @IBOutlet weak var tripCollectionView: UICollectionView!
    @IBOutlet weak var notTripsLabel: UILabel!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.delegate = self
        tripCollectionView.delegate = self
        tripCollectionView.dataSource = self
        if trips.isEmpty {
            DatabaseManager.instance.getTripsFromDatabase { (trips) in
                if !trips.isEmpty {
                    self.trips = trips
                }
            }
            tripCollectionView.reloadData()
        }
    }

    @IBAction func touchAddTripButton(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let addTripViewController = storyboard.instantiateViewController(withIdentifier: "addTripViewController") as! AddTripViewController
        addTripViewController.delegate = self
        present(addTripViewController, animated: true)
    }
    
    //MARK: Private
    private func configHidden() {
        if trips.isEmpty {
            tripCollectionView.isHidden = true
            notTripsLabel.isHidden = false
        } else {
            tripCollectionView.isHidden = false
            notTripsLabel.isHidden = true
        }
    }
}

extension TripsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UINavigationBarDelegate, AddTripViewControllerDelegate, StaysViewControllerDelegate {
    
    //MARK: PlacesViewControllerDelegate
    func touchStaysBackButton() {
        tripCollectionView.reloadData()
    }
    
    //MARK: AddTripViewControllerDelegate
    func touchAddTripSaveButton(with trip: Trip) {
        trips.append(trip)
        DatabaseManager.instance.saveTripToDatabase(trip)
        tripCollectionView.reloadData()
    }
    
    //MARK: UICollectionViewDelegate, UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        configHidden()
        return trips.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tripCell", for: indexPath) as! TripCollectionViewCell
        cell.setup(with: trips[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let placesViewController = storyboard.instantiateViewController(withIdentifier: "placesViewController") as? StaysViewController {
            placesViewController.trip = trips[indexPath.row]
            placesViewController.delegate = self
            present(placesViewController, animated: false, completion: nil)
        }
    }
    
    //MARK: UINavigationBarDelegate
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }
    
}
