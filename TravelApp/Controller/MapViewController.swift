//
//  MapViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate: class {
    func touchMapSaveButton(with value: String)
}

class MapViewController: UIViewController {

    weak var delegate: MapViewControllerDelegate?
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let minskPlace = Place()
        minskPlace.name = "Minsk"
        minskPlace.latitude = 53.925
        minskPlace.longitude = 27.508
        
        let moscowPlace = Place()
        moscowPlace.name = "Moscow"
        moscowPlace.latitude = 55.668
        moscowPlace.longitude = 37.689
        
        let minskAnnotation = MKPointAnnotation()
        minskAnnotation.coordinate = minskPlace.coordinate
        let moscowAnnotation = MKPointAnnotation()
        moscowAnnotation.coordinate = moscowPlace.coordinate
        
        mapView.addAnnotation(minskAnnotation)
        mapView.addAnnotation(moscowAnnotation)
    }
    
    //MARK: Action
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        if let location = mapView.annotations.first {
        delegate?.touchMapSaveButton(with: "(\(location.coordinate.latitude), \(location.coordinate.longitude))")
        }
        dismiss(animated: true, completion: nil)
    }
}
