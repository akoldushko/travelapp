//
//  AddTripViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

protocol AddTripViewControllerDelegate: class {
    func touchAddTripSaveButton(with trip: Trip)
}

class AddTripViewController: UIViewController {

    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    weak var delegate: AddTripViewControllerDelegate?
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Listen for keyboard events
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
//    deinit {
//        //Stop listening for keyboard hide/show events
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
//    }
    
    //MARK:
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: Keyboard Methods
//    @objc func keyboardWillChange(notification: Notification) {
//        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
//            return
//        }
//        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
//            view.frame.origin.y = -keyboardRect.height
//        } else {
//            view.frame.origin.y = 0
//        }
//    }

    //MARK: Action
    @IBAction func touchAddButton(_ sender: UIButton) {
        if let country = countryTextField.text {
            let trip = Trip()
            trip.country = country
            if let description = descriptionTextField.text {
                trip.tripDescription = description
            }
            delegate?.touchAddTripSaveButton(with: trip)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func touchCancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
