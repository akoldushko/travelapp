//
//  PlacesViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import RealmSwift

protocol StaysViewControllerDelegate: class {
    func touchStaysBackButton()
}

class StaysViewController: UIViewController {
   
    var trip: Trip = Trip()
    weak var delegate: StaysViewControllerDelegate?
    @IBOutlet weak var staysTableView: UITableView!
    @IBOutlet weak var notStaysLabel: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.delegate = self
        if let country = trip.country {
            navigationBar.topItem?.title = country
        }
    }
    
    //MARK: Action
    @IBAction func backButtonClicked(_ sender: UIButton) {
        delegate?.touchStaysBackButton()
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addPlaceButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addPlaceViewController = storyboard.instantiateViewController(withIdentifier: "AddPlaceViewController") as! AddStayViewController
        addPlaceViewController.delegate = self
        present(addPlaceViewController, animated: false, completion: nil)
    }
    
    //MARK: Private
    private func configHidden() {
        if trip.stays.isEmpty {
            staysTableView.isHidden = true
            notStaysLabel.isHidden = false
        } else {
            staysTableView.isHidden = false
            notStaysLabel.isHidden = true
        }
    }
   
    /*
     метод для задания высоты
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 87.0
    }
 */
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        <#code#>
//    }
}

extension StaysViewController: AddStayViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate {
    
    //MARK: UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stayCell", for: indexPath) as! StayCell
        cell.setup(with: trip.stays[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configHidden()
        return trip.stays.count
    }
    
    //MARK: UINavigationBarDelegate
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }
    
    //MARK: AddStayViewControllerDelegate
    func touchAddStayButton(with stay: Stay) {
        DatabaseManager.instance.updateTrip(trip, with: stay)
        staysTableView.reloadData()
    }
}
