//
//  TripCollectionViewCell.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

class TripCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tripView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet var raitingCollectionImage: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tripView.layer.cornerRadius = 5
    }
    
    func setup (with trip: Trip) {
        if let description = trip.tripDescription {
            descriptionLabel.text = description
        }
        if let country = trip.country {
            countryLabel.text = country
        }
        //print("\(trip.country) = \(trip.raiting) - \(trip.stays.first?.rating)")
        ratingCalculation(trip)
    }
    
    private func ratingCalculation(_ trip: Trip) {
        var sumOfRatings = 0
        for stay in trip.stays {
            if let ratingStay = stay.rating {
                if let rating = Int(ratingStay) {
                    sumOfRatings += rating
                }
            }
        }
        if sumOfRatings != 0 {
            let rating = sumOfRatings / trip.stays.count
            for index in 0..<rating {
                raitingCollectionImage[index].image = UIImage(named: "icon_star_gold")
            }
        }
    }
}
