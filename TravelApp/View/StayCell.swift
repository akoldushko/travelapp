//
//  TripCell.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/1/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

class StayCell: UITableViewCell {

    @IBOutlet weak var tripView: UIView!
    @IBOutlet var ratingImageCollection: [UIImageView]!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var transportImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tripView.layer.cornerRadius = 5
        transportImageView.layer.cornerRadius = transportImageView.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(with stay: Stay) {
        if let city = stay.city {
            cityLabel.text = city
        }
        if let description = stay.stayDescription {
            descriptionLabel.text = description
        }
        if let sum = stay.sum {
            sumLabel.text = sum
        }
        
        switch stay.transport  {
        case .car:
            transportImageView.image = UIImage(named: "icon_car")
        case .plane:
            transportImageView.image = UIImage(named: "icon_plane")
        case .train:
            transportImageView.image = UIImage(named: "icon_train")
        }
        
        if let ratingString = stay.rating {
            if let rating = Int(ratingString) {
                for index in 0..<rating {
                    ratingImageCollection[index].image = UIImage(named: "icon_star_gold")
                }
            }
        }
    }

}
