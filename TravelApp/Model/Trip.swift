//
//  Trip.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import RealmSwift

class Trip: Object {
    @objc dynamic var tripId = UUID().uuidString
    @objc dynamic var country: String?
    @objc dynamic var tripDescription: String?
    //@objc dynamic var raiting = 0
    let stays = List<Stay>()
//    {
//        didSet {
//            var sumOfRatings = 0
//            for stay in stays {
//                if let rating = stay.rating {
//                    if let intRating = Int(rating) {
//                        sumOfRatings += intRating
//                    }
//                }
//            }
//            if sumOfRatings != 0 {
//                raiting = sumOfRatings / stays.count
//            }
//        }
//    }
    
    override static func primaryKey() -> String? {
        return "tripId"
    }
//    var stays: [Stay]? {
//        didSet {
//            var sumOfRatings = 0
//            if let stays = stays {
//                for stay in stays {
//                    if let rating = stay.rating {
//                        if let intRating = Int(rating) {
//                            sumOfRatings += intRating
//                        }
//                    }
//                }
//                if sumOfRatings != 0 {
//                    raiting = sumOfRatings / stays.count
//                }
//            }
//        }
//    }
}
