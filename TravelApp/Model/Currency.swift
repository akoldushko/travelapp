//
//  Currency.swift
//  TravelApp
//
//  Created by Артем Хохлов on 12/1/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation

enum Currency: String {
    case dollar = "USD"
    case euro = "EUR"
    case ruble = "RUB"
    
    func sign() -> String {
        switch self {
        case .dollar:
            return "$"
        case .euro:
            return "€"
        case .ruble:
            return "₽"
        }
    }
}
