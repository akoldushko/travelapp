//
//  Travel.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import RealmSwift

class Stay: Object {
    @objc dynamic var city: String?
    @objc dynamic var stayDescription: String?
    @objc dynamic var rating: String?
    @objc dynamic var sum: String?
    @objc dynamic var realmTransport = Transport.car.rawValue
    var transport: Transport {
        get {
            return Transport(rawValue: realmTransport)!
        }
        set {
            realmTransport = newValue.rawValue
        }
    }
    @objc dynamic var location: String?
}
