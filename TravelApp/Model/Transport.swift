//
//  Transport.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/30/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation

enum Transport: String {
    case plane = "✈️"
    case train = "🚂"
    case car = "🚙"
}
