//
//  Place.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import MapKit

class Place {
    var name: String = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D.init(latitude: self.latitude, longitude: self.longitude)
    }
    
}
